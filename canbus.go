package canbus

import (
	"encoding/binary"
	"errors"
	"io"
	"net"

	"golang.org/x/sys/unix"
)

var (
	DataTooBig   = errors.New("canbus: data too big")
	ReadTimeout  = errors.New("canbus: read timeout")
	WriteTimeout = errors.New("canbus: write timeout")
	SocketError  = errors.New("canbus: socket error")
)

type device struct {
	fd int
}

type Socket struct {
	iface *net.Interface
	addr  *unix.SockaddrCAN
	dev   device
}

func New() (s *Socket, err error) {
	var fd int
	if fd, err = unix.Socket(unix.AF_CAN, unix.SOCK_RAW, unix.CAN_RAW); err != nil {
		return
	}
	s = &Socket{dev: device{fd}}
	return
}

func (s *Socket) Close() error {
	return unix.Close(s.dev.fd)
}

func (s *Socket) Bind(addr string) (err error) {
	var iface *net.Interface
	if iface, err = net.InterfaceByName(addr); err != nil {
		return
	}

	s.iface = iface
	s.addr = &unix.SockaddrCAN{Ifindex: s.iface.Index}

	return unix.Bind(s.dev.fd, s.addr)
}

func (s *Socket) Name() string {
	if s.iface == nil {
		return "N/A"
	}
	return s.iface.Name
}

const frameSize = 16

/*
type frame struct {
	ID   uint32
	Len  byte
	_    [3]byte
	Data [8]byte
}
*/

func (s *Socket) SendEFF(id uint32, data []byte) (int, error) {
	if len(data) > 8 {
		return -1, DataTooBig
	}

	frame := make([]byte, frameSize)
	id &= unix.CAN_EFF_MASK
	id |= unix.CAN_EFF_FLAG
	binary.LittleEndian.PutUint32(frame[:4], id)
	frame[4] = byte(len(data))
	copy(frame[8:], data)

	return s.dev.Write(frame)
}

func (s *Socket) Recv() (id uint32, data []byte, err error) {
	frame := make([]byte, frameSize)
	if _, err = io.ReadFull(s.dev, frame); err != nil {
		return
	}

	id = binary.LittleEndian.Uint32(frame[:4])
	if id&unix.CAN_EFF_FLAG != 0 {
		id &= unix.CAN_EFF_MASK
	} else {
		id &= unix.CAN_SFF_MASK
	}
	data = make([]byte, frame[4])
	copy(data, frame[8:])

	return
}

const readTimeout = 1000

//const writeTimeout = 1000

func (d device) Read(data []byte) (int, error) {
	fds := []unix.PollFd{{Fd: int32(d.fd), Events: unix.POLLIN}}
	n, err := unix.Poll(fds, readTimeout)
	if err != nil {
		return -1, err
	}
	if n == 0 {
		return -1, ReadTimeout
	}
	if fds[0].Revents&unix.POLLIN != 0 {
		return unix.Read(d.fd, data)
	}
	return -1, SocketError
}

func (d device) Write(data []byte) (int, error) {
	return unix.Write(d.fd, data)
}
